# This file is part of clickPublish: Content Planner.
#
# Copyright (C) 2023  CV Java Multi Mandiri
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from odoo import api, fields, models

class Schedule(models.Model):
    _name = 'schedule'
    _description = 'Content Planner'
    _inherit = [
        'mail.activity.mixin',
        'mail.thread',
    ]

    name = fields.Char(
        readonly=True,
        required=True,
        states={'draft': [('readonly', False)]},
    )
    description = fields.Html(
        readonly=True,
        required=True,
        states={'draft': [('readonly', False)]},
    )
    planned_published_date = fields.Datetime(
        readonly=True,
        required=True,
        states={'draft': [('readonly', False)]},
    )
    media_ids = fields.Many2many(
        comodel_name='clickpublish.website',
        readonly=True,
        required=True,
        states={'draft': [('readonly', False)]},
    )
    type_id = fields.Many2one(
        comodel_name="clickpublish.article.type",
        readonly=True,
        states={'draft': [('readonly', False)]},
        string="Content type",
    )
    assignee_ids = fields.Many2many(
        comodel_name='hr.employee',
        readonly=True,
        required=True,
        states={'draft': [('readonly', False)]},
    )
    request_id = fields.Many2one(
        comodel_name='request',
        readonly=True,
        states={'draft': [('readonly', False)]},
    )
    project_id = fields.Many2one(
        comodel_name = 'project.project',
        readonly=True,
        states={'draft': [('readonly', False)]},
        string = 'Project',
    )
    state = fields.Selection(
        selection = [
            ('draft', 'Draft'),
            ('scheduled', 'Scheduled'),
            ('in_progress', 'In progress'),
            ('done', 'Done'),
        ],
        default = "draft",
        required = True,
    )
    publication_ids = fields.One2many(
        comodel_name = 'clickpublish.article',
        inverse_name = 'schedule_id',
    )

    def action_schedule(self):
        self.write({
            'name': self.env['ir.sequence'].next_by_code('schedule.sequence') + " - " + self.name,
            'state': 'scheduled',
        })

        # Check if there are any project to be related to.
        if self.project_id:
            media_string = "<ul>"

            for media in self.media_ids:
                media_string += "<li>" + media.display_name + "</li>"

            media_string += "</ul>"

            description = """
            <p><strong>Deskripsi</strong></p>

            %s

            <hr/>

            <p><strong>Media Publikasi</strong></p>

            %s

            <hr/>

            <p><strong>Jenis Konten</strong></p>

            %s

            """ %(self.description, media_string, self.type_id.name)

            task_metadata = {
                'name': 'Task for content publication number ' + self.name,
                'description': description,
                'date_deadline': self.planned_published_date,
                'project_id': self.project_id.id,
            }

            # Check if there are any request to be related to.
            if self.request_id:
                description += """
                <hr/>

                <p><strong>Permintaan:</strong> %s</p>

                %s
                """ %(self.request_id.name, self.request_id.description)

                task_metadata.update({
                    'request_id': self.request_id.id,
                    'description': description,
                })

            # Create task
            task = self.env['project.task'].create(task_metadata)

        # Check if there are any person to be related to.
        if self.assignee_ids:
            for record in self.assignee_ids:
                partner = record.user_id.partner_id
                user = record.user_id.id

                self.activity_schedule(
                    activity_type_id = self.env.ref('mail.mail_activity_data_todo').id,
                    date_deadline = self.planned_published_date,
                    summary = 'New content publishing schedule',
                    note = 'A content publishing schedule has been created for you.',
                    user_id = user,
                )

            if self.project_id:
                users = self.assignee_ids.mapped('user_id').ids

                task_metadata.update({
                    'task_id': [(4, task.id)]
                })

                task.update({
                    'user_ids': [(5, 0, 0)]
                })

                task.update({
                    'user_ids': [(6, 0, users)]
                })

                for assignee in self.assignee_ids:
                    partner = assignee.user_id.partner_id
                    user = assignee.user_id.id

                    if partner:
                        self.message_subscribe(partner_ids=[partner.id])

                        task.activity_schedule(
                            act_type_xmlid = 'mail.mail_activity_data_todo',
                            date_deadline = record.planned_published_date,
                            summary = 'New content publishing schedule',
                            note = 'A content publishing schedule has been created for you.',
                            user_id = user,
                        )

        self.log_note('This publication planning has been scheduled.')

    def action_process_request(self):
        self.write({
            'state': 'in_progress',
        })

        self.activity_ids._action_done(
            feedback = 'This publication schedule has been processed.',
        )

        self.log_note('This publication schedule has been processed.')

        return True

    def action_done(self):
        self.write({
            'state': 'done'
        })

        self.log_note('This publication schedule has been completed.')

    def log_note(self, message):
        for document in self:
            document.message_post(body=message)
