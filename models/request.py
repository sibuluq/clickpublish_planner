# This file is part of clickPublish: Content Planner.
#
# Copyright (C) 2023  CV Java Multi Mandiri
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from odoo import _, api, fields, models

class Request(models.Model):
    _name = 'request'
    _description = 'Publication Request'
    _inherit = [
        'mail.activity.mixin',
        'mail.thread',
    ]

    name = fields.Char(
        readonly = True,
        required = True,
        states = {'draft': [('readonly', False)]},
        string = _('Summary'),
    )
    start_date = fields.Date(
        help = """Kamu bisa memilih rentang waktu untuk mempublikasi kontennya
            atau memilih satu hari tertentu. Untuk memilih satu hari tertentu,
            klik dua kali pada hari yang diinginkan.""",
        readonly = True,
        required = True,
        states = {'draft': [('readonly', False)]},
        string = _('Start date'),
    )
    end_date = fields.Date(
        readonly = True,
        required = True,
        states = {'draft': [('readonly', False)]},
        string = _('End date'),
    )
    state = fields.Selection(
        selection = [
            ('draft', _('Draft')),
            ('submitted', _('Submitted')),
            ('processed', _('Processed')),
            ('done', _('Done')),
        ],
        default = 'draft',
        required = True,
        string = _('State'),
    )
    description = fields.Html(
        readonly = True,
        states = {'draft': [('readonly', False)]},
        string=_('Description')
    )
    schedule_ids = fields.One2many(
        comodel_name = 'schedule',
        inverse_name = 'request_id',
        string = _('Schedule'),
    )
    schedule_count = fields.Integer(
        compute='_compute_schedule_count',
    )
    task_ids = fields.One2many(
        comodel_name = 'project.task',
        inverse_name = 'request_id',
    )
    publication_ids = fields.One2many(
        comodel_name = 'clickpublish.article',
        compute = '_compute_publication_ids',
    )
    published_ids = fields.One2many(
        comodel_name = 'clickpublish.article',
        compute = '_compute_published_ids',
    )
    publication_count = fields.Integer(
        compute='_compute_publication_count',
    )
    type_ids = fields.Many2many(
        comodel_name = 'clickpublish.article.type',
        readonly = True,
        states = {'draft': [('readonly', False)]},
        string = _('Content type'),
    )

    def _compute_schedule_count(self):
        for record in self:
            total = len(self.env['schedule'].search(['&', ('request_id', '=', record.id), ('state', '!=', 'draft')]))

            record.schedule_count = total

    @api.depends('task_ids.publication_ids')
    def _compute_publication_ids(self):
        for record in self:
            publication_ids = record.task_ids.mapped('publication_ids')
            record.publication_ids = publication_ids

    @api.depends('schedule_ids.publication_ids')
    def _compute_published_ids(self):
        for record in self:
            publication_ids = record.schedule_ids.mapped('publication_ids')
            record.published_ids = publication_ids

    def _compute_publication_count(self):
        for record in self:
            total = len(self.env['clickpublish.article'].search(['|', ('schedule_id.request_id', '=', self.id), ('task_id.request_id', '=', self.id)]))

            record.publication_count = total

    def action_done(self):
        self.write({
            'state': 'done'
        })

        self.log_note('Hi %s, your publication request has been completed.' % (self.create_uid.name))

    def action_process_request(self):
        self.write({
            'state': 'processed',
        })

        self.set_activity_done('Publication request has been processed.')
        self.log_note('This publication request has been processed.')

        return True

    def action_submit(self):
        self.write({
            'state': 'submitted',
        })

        self.send_task(
            self.start_date,
            'Content publication request',
            """The request for content publication has been submitted. Please
                process it immediately.""",
            'clickpublish_planner.schedule_group_planner',
        )

        self.log_note('This publication request has been submitted.')

    def log_note(self, message):
        for document in self:
            document.message_post(body=message)

    def send_task(self, starting_date, summary, message, user_group):
        group = self.env.ref(user_group)

        for user in group.users:
            self.activity_schedule(
                activity_type_id = self.env.ref('mail.mail_activity_data_todo').id,
                date_deadline = starting_date,
                note = message,
                summary = summary,
                user_id = user.id,
            )

    def set_activity_done(self, message):
        self.activity_ids._action_done(
            feedback = message,
        )

    def get_plan(self):
        self.ensure_one()

        return {
            'name': 'Content Planner',
            'context': {'default_request_id': self.id},
            'domain': ['&', ('request_id', '=', self.id), '|', ('state', '!=', 'draft'), ('create_uid', '=', self.env.uid)],
            'res_model': 'schedule',
            'type': 'ir.actions.act_window',
            'view_mode': 'calendar,tree,form'
        }

    def get_published_content(self):
        self.ensure_one()

        return {
            'name': 'Published Contents',
            'context': {'create': False},
            'domain': ['|', ('schedule_id.request_id', '=', self.id), ('task_id.request_id', '=', self.id)],
            'res_model': 'clickpublish.article',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree',
        }
