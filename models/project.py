# This file is part of clickPublish: Content Planner.
#
# Copyright (C) 2023  CV Java Multi Mandiri
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from odoo import fields, models

class Project(models.Model):
    _inherit = 'project.project'

    schedule_ids = fields.One2many(
        comodel_name = 'schedule',
        inverse_name = 'project_id',
    )

class ProjectTask(models.Model):
    _inherit = 'project.task'

    publication_ids = fields.One2many(
        comodel_name = 'clickpublish.article',
        inverse_name = 'task_id',
    )

    request_id = fields.Many2one(comodel_name='request')
