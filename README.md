# clickPublish: Content Planner

clickPublish: Content Planning is an app designed to help content creator teams
manage their content publishing schedule. With clickPublish, you can easily plan,
organize, and schedule your content for publishing on various platforms. With
clickPublish, you can create a content calendar that allows you to see all your
upcoming posts at a glance. You can also set deadlines, assign tasks to team
members, and collaborate on content creation.

## Credits

The development of this module is supported by [CV Java Multi Mandiri](https://jvm.co.id)
and carried out by [clickoding](https://clickoding.id) as a business unit engaged
in the software development industry.

### Contributors

- Sri Hayyu Ulfa Naufal <sri.naufal@jvm.co.id">, CV Java Multi Mandiri
- Sunu Haeriadi <sunu.haeriadi@jvm.co.id">, CV Java Multi Mandiri

### Maintainer

This module is maintained by clickoding.

clickoding is a software house that develops business solution applications for
Indonesian business people.

clickoding is proud to be part of JVM.
