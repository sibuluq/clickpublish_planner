# clickPublish: Content Planner Development Guides

## Design

### Use case diagram

```plantuml
@startuml
skinparam actorStyle awesome
left to right direction

package clickPublish {
    usecase "Browse content schedule" as UC1
    usecase "Create content schedule" as UC2
    usecase "Work on a content schedule" as UC3
    usecase "Request content publication" as UC4
}

actor "Content Planner" as A1
actor "clickPublish Officer" as A2
actor "Employee" as A3

A1 -- UC1
A1 -- UC2
A1 -- UC3
A1 -- UC4

UC1 -- A2
UC3 -- A2

A3 -- UC4

@enduml
```

### Entity relationship diagram

#### Overview v.1.0.0

```plantuml
@startuml
skinparam linetype ortho
hide circle
hide empty members

entity "clickpublish.article" as rtcl {
    request_id : many2one "request"
    task_id : many2one "project.task"
}

entity "clickpublish.article.type" as typ {}

entity "project.project" as prj {
    schedule_ids : one2many "schedule"
}

entity "project.task" as tsk {
    publication_ids : one2many "clickpublish.article"
    request_id : many2one "request"
}

entity request {
    # create_date : datetime <<automated>>
    # create_uid : many2one "res.users" <<automated>>
    --
    * name : char
    * start_date : date
    * end_date : date
    * state : selection default "submitted"
    description : html
    schedule_ids : one2many "schedule"
    task_ids : one2many "project.task"
    publication_ids : one2many "clickpublish.article" <<computed>>
    type_ids : many2many "clickpublish.article.type"
}

entity schedule {
    # create_date : datetime <<automated>>
    # create_uid : many2one "res.users" <<automated>>
    --
    * name : char <<generated>>
    * description : html
    * planned_published_date : datetime
    * media_ids : many2many "clickpublish.website"
    type_id : many2one "clickpublish.artcile.type"
    * assignee_ids : many2many "hr.employee"
    request_id : many2one "request"
    * project_id : many2one "project.project"
    * state : selection default "draft"
}

' Article model
clickpublish.article <|-- rtcl
tsk |o--o{ rtcl

' Project model
project.project <|-- prj

' Task model
project.task <|-- tsk

' Request model
res.users ||--o{ request
request |o--o{ schedule
request |o--o{ tsk
request |o--o{ rtcl
request }--{ typ

' Schedule model
res.users ||--o{ schedule
schedule }--{ clickpublish.website
typ |o--o{ schedule
hr.employee ||--o{ schedule
prj ||--o{ schedule

@enduml
```

#### v.1.1.0

##### Overview v.1.1.0

```plantuml
@startuml
skinparam linetype ortho
hide circle
hide empty members

entity "clickpublish.article" as rtcl {
    request_id : many2one "request"
    schedule_id : many2one "schedule"
    task_id : many2one "project.task"
}

entity "clickpublish.article.type" as typ {}

entity "project.project" as prj {
    schedule_ids : one2many "schedule"
}

entity "project.task" as tsk {
    publication_ids : one2many "clickpublish.article"
    request_id : many2one "request"
}

entity request {
    # create_date : datetime <<automated>>
    # create_uid : many2one "res.users" <<automated>>
    --
    * name : char
    * start_date : date
    * end_date : date
    * state : selection default "submitted"
    description : html
    schedule_ids : one2many "schedule"
    schedule_count : integer <<computed>>
    task_ids : one2many "project.task"
    publication_ids : one2many "clickpublish.article" <<computed>>
    published_ids : one2many "clickpublish.article" <<computed>>
    publication_count : integer <<computed>>
    type_ids : many2many "clickpublish.article.type"
}

entity schedule {
    # create_date : datetime <<automated>>
    # create_uid : many2one "res.users" <<automated>>
    --
    * name : char <<generated>>
    * description : html
    * planned_published_date : datetime
    * media_ids : many2many "clickpublish.website"
    type_id : many2one "clickpublish.artcile.type"
    * assignee_ids : many2many "hr.employee"
    request_id : many2one "request"
    project_id : many2one "project.project"
    * state : selection default "draft"
    publication_ids : one2many "clickpublish.article" <<computed>>
}

' Article model
clickpublish.article <|-- rtcl
tsk |o--o{ rtcl

' Project model
project.project <|-- prj

' Task model
project.task <|-- tsk

' Request model
res.users ||--o{ request
request |o--o{ schedule
request |o--o{ tsk
request |o--o{ rtcl
request }--{ typ

' Schedule model
res.users ||--o{ schedule
schedule }--{ clickpublish.website
typ |o--o{ schedule
hr.employee ||--o{ schedule
prj ||--o{ schedule
schedule |o--o{ rtcl

@enduml
```

##### Perubahan yang dilakukan

Menambahkan relasi antara `schedule` dengan `clickpublish.article`.

```plantuml
@startuml
skinparam linetype ortho
hide circle
hide empty members

entity schedule {
    # create_date : datetime <<automated>>
    # create_uid : many2one "res.users" <<automated>>
    --
    * name : char <<generated>>
    * description : html
    * planned_published_date : datetime
    * media_ids : many2many "clickpublish.website"
    type_id : many2one "clickpublish.artcile.type"
    * assignee_ids : many2many "hr.employee"
    request_id : many2one "request"
    project_id : many2one "project.project"
    * state : selection default "draft"
    publication_ids : one2many "clickpublish.article" <<computed>>
}

entity "clickpublish.article" as rtcl {
    request_id : many2one "request"
    schedule_id : many2one "schedule"
    task_id : many2one "project.task"
}

' Schedule model
schedule |o--o{ rtcl
@enduml
```

Menambahkan computed counting pada `request` agar dapat menapilkan info pada
_smart button_.


```plantuml
@startuml
skinparam linetype ortho
hide circle
hide empty members

entity request {
    ...
    schedule_ids : one2many "schedule"
    schedule_count : integer <<computed>>
    ...
    published_ids : one2many "clickpublish.article" <<computed>>
    publication_count : integer <<computed>>
    ...
}
@enduml
```
