# This file is part of clickPublish: Content Planner.
#
# Copyright (C) 2023  CV Java Multi Mandiri
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

{
    "name": "clickPublish: Content Planner",
    "summary": "Easily plan, organize, and schedule your content publishing.",
    "version": "15.0.1.1.0",
    "category": "Sales",
    "website": "https://gitlab.com/jvmdev/odoo/15/clickpublish-planner",
    "author": "CV Java Multi Mandiri",
    "maintainers": "clickoding",
    "license": "GPL-3",
    "depends": [
        "hr",
        "mail",
        "project",
        "clickpublish",
    ],
    "data": [
        "data/schedule_data.xml",
        "security/schedule_security.xml",
        "security/request_security.xml",
        "security/ir.model.access.csv",
        "views/article_views.xml",
        "views/project_views.xml",
        "views/schedule_views.xml",
        "views/request_views.xml",
    ],
}
